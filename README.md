1. Sett appropriate values for below used args (projectId, staging, temp locations, etc.)

2. Sample data csv file (`sample1.csv`) is present in directory: `data_management/dataflow-sample-inputs-csvs`

### Run with:

```
mvn clean compile exec:java \
        -Dexec.mainClass="com.webengage.gcp.test_dataflow.BigQueryStreamFromGcsFile" \
        -Dexec.args="--project=test-webengage 
        			--input=gs://test-dataflow-bucket-1/input/sample1.csv \
					--stagingLocation=gs://test-dataflow-bucket-1/staging-files \
					--tempLocation=gs://test-dataflow-bucket-1/temp-files \
					--output=test-webengage:dataflow_test_dataset.test_table1 \
					--runner=BlockingDataflowPipelineRunner" -o
```
