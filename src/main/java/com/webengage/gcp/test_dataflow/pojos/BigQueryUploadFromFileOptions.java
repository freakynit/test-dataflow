package com.webengage.gcp.test_dataflow.pojos;

import com.google.cloud.dataflow.sdk.options.Default;
import com.google.cloud.dataflow.sdk.options.Description;
import com.google.cloud.dataflow.sdk.options.PipelineOptions;

public interface BigQueryUploadFromFileOptions extends PipelineOptions {
    @Description("Project to run under")
//    @Default.String("test-webengage")
    String getProject();
    void setProject(String value);

    @Description("Path of the file to read from. gs file path should be specified gs://<bucket>/...")
    @Default.String("gs://dataflow-test-files/sample1.csv")
    String getInput();
    void setInput(String value);

    @Description("BigQuery dataset to write to, specified as <project_id>:<dataset_id>.table_id. The dataset must already exist.")
//    @Default.String("test-webengage:dataflow_test_dataset.test_table1")
    String getOutput();
    void setOutput(String value);

//    @Default.String("test-webengage:dataflow_temp_dataset")
    String getTempDatasetId();
    void setTempDatasetId(String value);

//    @Default.String("gs://test-dataflow-bucket-1/staging-files")
    String getStagingLocation();
    void setStagingLocation(String value);

//    @Default.String("gs://test-dataflow-bucket-1/temp-files")
    String getTempLocation();
    void setTempLocation(String value);
}
