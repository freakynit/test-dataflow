package com.webengage.gcp.test_dataflow;

import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;
import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.io.BigQueryIO;
import com.google.cloud.dataflow.sdk.io.TextIO;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;
import com.google.cloud.dataflow.sdk.transforms.DoFn;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.values.PCollection;
import com.webengage.gcp.test_dataflow.pojos.BigQueryUploadFromFileOptions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BigQueryStreamFromGcsFile {
    static class CsvToBqTableRow extends DoFn<String, TableRow> {
        @Override
        public void processElement(ProcessContext c) throws Exception {
            try {
                String line = c.element();
                String[] columnValues = line.split(",");

                TableRow tableRow = new TableRow();
                if(columnValues.length > 1) {
                    tableRow.set("license_code", columnValues[0]);
                    tableRow.set("event_name", columnValues[1]);
                } else {
                    tableRow.set("license_code", "fallback_lc");
                    tableRow.set("event_name", "fallback_ev_" + new Date().getTime());
                }

                c.output(tableRow);
            } catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    public static TableSchema getDestinationTableSchema(){
        List<TableFieldSchema> fields = new ArrayList<>();
        fields.add(new TableFieldSchema().setName("license_code").setType("STRING"));
        fields.add(new TableFieldSchema().setName("event_name").setType("STRING"));

        return new TableSchema().setFields(fields);
    }

    public static void main(String[] args) {
        BigQueryUploadFromFileOptions options = PipelineOptionsFactory.fromArgs(args).withValidation().as(BigQueryUploadFromFileOptions.class);
        TableSchema tableSchema = getDestinationTableSchema();

        Pipeline p = Pipeline.create(options);
        // Imp: Bug: https://github.com/GoogleCloudPlatform/DataflowJavaSDK/issues/356
        // Anyways, above bug is not relevant to this test code, so, ignore it
        PCollection<String> csvLinesPCollection = p.apply(TextIO.Read.named("test-dataflow-bigquery-stream").from(options.getInput()));

        PCollection<TableRow> bqTableRowsPCollection = csvLinesPCollection.apply(ParDo.of(new CsvToBqTableRow()));

        // ToDo: We need to create dataset dynamically based on value of "license_code" column
        // Need to call: "DatasetUtils.createDataset()" from correct place
        bqTableRowsPCollection.apply(BigQueryIO.Write.to(options.getOutput())
                        .withSchema(tableSchema)
                        .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED)
                        .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_APPEND));

        p.run();
    }
}
