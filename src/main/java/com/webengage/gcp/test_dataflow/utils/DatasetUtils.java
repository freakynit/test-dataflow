package com.webengage.gcp.test_dataflow.utils;

import com.google.api.services.bigquery.Bigquery;
import com.google.api.services.bigquery.model.Dataset;
import com.google.api.services.bigquery.model.DatasetReference;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class DatasetUtils {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(DatasetUtils.class);

    public static Dataset createDataset(Bigquery bigquery, String projectId, String datasetId, String datasetLocation) throws IOException {
        DatasetReference datasetReference = new DatasetReference();
        datasetReference.setProjectId(projectId);
        datasetReference.setDatasetId(datasetId);

        Dataset dataset = new Dataset();
        dataset.setDatasetReference(datasetReference);
        dataset.setLocation(datasetLocation);

        try {
            Bigquery.Datasets.Insert insert = bigquery.datasets().insert(projectId, dataset);
            return insert.execute();
        } catch(IOException e){
            logger.error(e.getMessage(), e);
            throw e;
        }
    }
}
